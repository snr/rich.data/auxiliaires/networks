import os

# from src.pipeline import pipeline
from src.A_sql2graph import sql2graph_pipeline
from src.B_analysis_base import analysis_base_pipeline
from src.utils import DATA


if __name__ == "__main__":
    if not os.path.isdir(DATA):
        os.makedirs(DATA)
    # pipeline()
    sql2graph_pipeline()
    analysis_base_pipeline()
    print("TODO: SÉPARER LES DIFFÉRENTES ÉTAPES PAR UN CLI")

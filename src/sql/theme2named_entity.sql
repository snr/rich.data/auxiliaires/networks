-- the full, final query creates a table with all the
-- relationship between all named entities (id and name for each named_entity)
-- + the weight of the edge between them (the number of relationships
-- between the 2 named entities.)
--
-- WARNING: the query creates a bi-directionnal relationship:
-- a row will be created from named_entityA to named_entityB, and another from
-- named_entityB to named_entityA. both rows will have the same weight and express
-- the same thing.
-- since we want to create a non-drectional graph, the relation
-- from named_entityB to named_entityA will need to be deleted.

-- iconography id mapped to json array of named_entity ids
WITH icono2named_entity_arr AS (
  SELECT r_iconography_named_entity.id_iconography,
         array_agg(r_iconography_named_entity.id_named_entity) AS id_named_entity
  FROM r_iconography_named_entity
  GROUP BY r_iconography_named_entity.id_iconography
),
-- theme id mapped to nested json array of named_entity ids (array of array of named_entity_id)
-- no need to pass by the Iconography table since `id_iconography` is present both in 
-- r_iconography_named_entity and r_iconography_theme
theme2named_entity_arr AS (
  SELECT r_iconography_theme.id_theme AS theme_id,
         json_agg(icono2named_entity_arr.id_named_entity) AS named_entity_id
  FROM r_iconography_theme
  JOIN icono2named_entity_arr
    ON r_iconography_theme.id_iconography = icono2named_entity_arr.id_iconography
  GROUP BY r_iconography_theme.id_theme
),
-- theme id mapped to named_entity id (an unnested version of the above query)
theme2named_entity_scalar AS (
  SELECT theme2named_entity_arr.theme_id,
         json_array_elements( json_array_elements( theme2named_entity_arr.named_entity_id ) -- 2 json_array_elements because `theme2named_entity_arr.named_entity_id` is of depth 2
                            )::text::int AS named_entity_id                               -- `::text::int` is a dirty fix to convert a json column containing only ints to int
  FROM theme2named_entity_arr
),
-- all of the relations between any theme and named entity + their weight
-- <theme_id> | <named_entity_id> | <number of relationships between them>
weighted_relations AS (
  SELECT theme2named_entity_scalar.theme_id,
         theme2named_entity_scalar.named_entity_id,
         COUNT (theme2named_entity_scalar.theme_id) AS weight
  FROM theme2named_entity_scalar
  GROUP BY theme2named_entity_scalar.theme_id, theme2named_entity_scalar.named_entity_id
  ORDER BY weight DESC
),
-- -- basic queries to get the name of named_entities and themes
named_entity_name AS (
  SELECT named_entity.id, named_entity.entry_name
  FROM named_entity
),
theme_name AS (
  SELECT theme.id, theme.entry_name
  FROM theme
),
-- -- basic queries to get the number of times a named_entity|theme is used to tag the iconography dataset
named_entity_count AS (
  SELECT r_iconography_named_entity.id_named_entity, COUNT(r_iconography_named_entity.id_named_entity)
  FROM r_iconography_named_entity
  GROUP BY r_iconography_named_entity.id_named_entity
),
theme_count AS (
  SELECT r_iconography_theme.id_theme, COUNT(r_iconography_theme.id_theme)
  FROM r_iconography_theme
  GROUP BY r_iconography_theme.id_theme
)
-- final query
SELECT weighted_relations.theme_id,
       theme_name.entry_name AS theme_name,
       theme_count.count AS theme_count,
       weighted_relations.named_entity_id,
       named_entity_name.entry_name AS named_entity_name,
       named_entity_count.count AS named_entity_count,
       weighted_relations.weight
FROM weighted_relations
LEFT JOIN theme_name
ON weighted_relations.theme_id = theme_name.id
LEFT JOIN named_entity_name
ON weighted_relations.named_entity_id = named_entity_name.id
LEFT JOIN theme_count
ON weighted_relations.theme_id = theme_count.id_theme
LEFT JOIN named_entity_count
ON weighted_relations.named_entity_id = named_entity_count.id_named_entity
ORDER BY weight DESC
;



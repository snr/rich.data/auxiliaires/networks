-- the full, final query creates a table with all the
-- relationship between all named entities (id and name for each named_entity)
-- + the weight of the edge between them (the number of relationships
-- between the 2 named entities.)
--
-- WARNING: the query creates a bi-directionnal relationship:
-- a row will be created from named_entityA to named_entityB, and another from
-- named_entityB to named_entityA. both rows will have the same weight and express
-- the same thing.
-- since we want to create a non-drectional graph, the relation
-- from named_entityB to named_entityA will need to be deleted.
--
-- output structure:
--
--
-- iconography id mapped to json array of named_entity ids
WITH icono2named_entity_arr AS (
  SELECT r_iconography_named_entity.id_iconography,
         array_agg(r_iconography_named_entity.id_named_entity) AS id_named_entity
  FROM r_iconography_named_entity
  GROUP BY r_iconography_named_entity.id_iconography
),
-- named_entity id mapped to nested json array of named_entity ids (array of array of named_entity_id)
named_entity2named_entity_arr AS (
  SELECT r_iconography_named_entity.id_named_entity AS named_entity_a_id,
         json_agg(icono2named_entity_arr.id_named_entity) AS named_entity_b_id
  FROM r_iconography_named_entity
  JOIN icono2named_entity_arr
    ON r_iconography_named_entity.id_iconography = icono2named_entity_arr.id_iconography
  GROUP BY r_iconography_named_entity.id_named_entity
),
-- named_entity id mapped to named_entity id (an unnested version of the above query)
named_entity2named_entity_scalar AS (
  SELECT named_entity2named_entity_arr.named_entity_a_id,
         json_array_elements(
           json_array_elements( named_entity2named_entity_arr.named_entity_b_id )
         )::text::int AS named_entity_b_id   -- `::text::int` is a dirty fix to convert a json column containing only ints to int
  FROM named_entity2named_entity_arr
),
-- all of the relations between any two named_entity_s + their weight
-- <id of the first named_entity> | <id of the second named_entity> | <number of relationships between the two named_entity_s>
weighted_relations AS (
  SELECT named_entity2named_entity_scalar.named_entity_a_id,
         named_entity2named_entity_scalar.named_entity_b_id,
         COUNT (named_entity2named_entity_scalar.named_entity_a_id) AS weight
  FROM named_entity2named_entity_scalar
  WHERE named_entity2named_entity_scalar.named_entity_a_id != named_entity2named_entity_scalar.named_entity_b_id  -- drop relations from a named_entity to itself
  GROUP BY named_entity2named_entity_scalar.named_entity_a_id, named_entity2named_entity_scalar.named_entity_b_id
  ORDER BY weight DESC
),
-- basic query to get the name of a named_entity
named_entity_name AS (
  SELECT named_entity.id, named_entity.entry_name
  FROM named_entity
),
-- basic query to get the number of times a named_entity is used to tag the iconography dataset
named_entity_count AS (
  SELECT r_iconography_named_entity.id_named_entity, COUNT(r_iconography_named_entity.id_named_entity)
  FROM r_iconography_named_entity
  GROUP BY r_iconography_named_entity.id_named_entity
)
-- final query
SELECT weighted_relations.named_entity_a_id,
       named_entity_a_name.entry_name AS named_entity_a_name,
       named_entity_a_count.count AS named_entity_a_count,
       weighted_relations.named_entity_b_id,
       named_entity_b_name.entry_name AS named_entity_b_name,
       named_entity_b_count.count AS named_entity_b_count,
       weighted_relations.weight
FROM weighted_relations
LEFT JOIN named_entity_name AS named_entity_a_name
ON weighted_relations.named_entity_a_id = named_entity_a_name.id
LEFT JOIN named_entity_name AS named_entity_b_name
ON weighted_relations.named_entity_b_id = named_entity_b_name.id
LEFT JOIN named_entity_count AS named_entity_a_count
ON weighted_relations.named_entity_a_id = named_entity_a_count.id_named_entity
LEFT JOIN named_entity_count AS named_entity_b_count
ON weighted_relations.named_entity_b_id = named_entity_b_count.id_named_entity
ORDER BY weight DESC
;



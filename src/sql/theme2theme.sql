-- the full, final query creates a table with all the 
-- relationship between all themes (id and name for each theme)
-- + the weight of the edge between them (the number of relationships 
-- between the 2 themes.)
--
-- WARNING: the query creates a bi-directionnal relationship:
-- a row will be created from themeA to themeB, and another from
-- themeB to themeA. both rows will have the same weight and express
-- the same thing.
-- since we want to create a non-drectional graph, the relation
-- from themeB to themeA will need to be deleted.
--
-- output structure:
-- theme_a_id::int | theme_a_name::text | theme_a_count::bigint | theme_b_id::int | theme_b_name::text | theme_b_count::bigint | weight::bigint
-- 1	             | "espace public"    |	1410                  |	0               |	"architecture"     |	1808                 |	1342
--
--
-- iconography id mapped to json array of theme ids
WITH icono2themearr AS (
  SELECT r_iconography_theme.id_iconography, 
         array_agg(r_iconography_theme.id_theme) AS id_theme
  FROM r_iconography_theme
  GROUP BY r_iconography_theme.id_iconography
),
-- theme id mapped to nested json array of theme ids (array of array of theme_id)
theme2themearr AS (
  SELECT r_iconography_theme.id_theme AS theme_a_id,
         json_agg(icono2themearr.id_theme) AS theme_b_id
  FROM r_iconography_theme 
  JOIN icono2themearr 
    ON r_iconography_theme.id_iconography = icono2themearr.id_iconography
  GROUP BY r_iconography_theme.id_theme
),
-- theme id mapped to theme id (an unnested version of the above query)
theme2themescalar AS (
  SELECT theme2themearr.theme_a_id,
         json_array_elements(
           json_array_elements( theme2themearr.theme_b_id )
         )::text::int AS theme_b_id   -- `::text::int` is a dirty fix to convert a json column containing only ints to int
  FROM theme2themearr
),
-- all of the relations between any two themes + their weight
-- <id of the first theme> | <id of the second theme> | <number of relationships between the two themes>
weighted_relations AS (
  SELECT theme2themescalar.theme_a_id, 
         theme2themescalar.theme_b_id,
         COUNT (theme2themescalar.theme_a_id) AS weight
  FROM theme2themescalar
  WHERE theme2themescalar.theme_a_id != theme2themescalar.theme_b_id  -- drop relations from a theme to itself
  GROUP BY theme2themescalar.theme_a_id, theme2themescalar.theme_b_id
  ORDER BY weight DESC
),
-- basic query to get the name of a theme
theme_name AS (
  SELECT theme.id, theme.entry_name 
  FROM theme
),
-- basic query to get the number of times a theme is used to tag the iconography dataset
theme_count AS (
  SELECT r_iconography_theme.id_theme, COUNT(r_iconography_theme.id_theme)
  FROM r_iconography_theme
  GROUP BY r_iconography_theme.id_theme
)
-- final query
SELECT weighted_relations.theme_a_id,
       theme_a_name.entry_name AS theme_a_name,
       theme_a_count.count AS theme_a_count,
       weighted_relations.theme_b_id,
       theme_b_name.entry_name AS theme_b_name,
       theme_b_count.count AS theme_b_count,
       weighted_relations.weight
FROM weighted_relations
LEFT JOIN theme_name AS theme_a_name 
ON weighted_relations.theme_a_id = theme_a_name.id
LEFT JOIN theme_name AS theme_b_name 
ON weighted_relations.theme_b_id = theme_b_name.id
LEFT JOIN theme_count AS theme_a_count 
ON weighted_relations.theme_a_id = theme_a_count.id_theme
LEFT JOIN theme_count AS theme_b_count 
ON weighted_relations.theme_b_id = theme_b_count.id_theme
;



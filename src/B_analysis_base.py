"""
basic analysis on the whole graph and on individual nodes.
write results to file
"""

from networkx.algorithms import community
from matplotlib import pyplot as plt
from pyvis.network import Network
import networkx as nx
import pandas as pd
import typing as t
import numpy as np
import netgraph
import os

from .utils import DATA, random_color


def read(fp) -> nx.Graph:
    """
    read a csv file into a pandas edgelist into a networkx graph.

    expected columns (given that `u`,`v` are 2 nodes in the graph
    and that `uv` is the edge connecting them):
    'source'      : node `u` (source node in an edge)
    'target'      : node `v` (target node in an edge)
    'weight'      : weight of the edge uv
    'title'       : title displayed on the edge `uv`
    'source_mass' : mass of `u` for gravity model of pyvis
    'target_mass' : mass of `v` for gravity model of pyvis
    'source_size' : size of the node `u`
    'target_size' : size of the node `v`
    """
    df = pd.read_csv(fp, sep="\t", index_col=0)

    # build a `df_node` to add node attributes
    df_source_nodes = (df[["source", "source_mass", "source_size"]]
                       .rename(columns={"source"      : "id",
                                        "source_mass" : "mass",
                                        "source_size" : "size" }) )
    df_target_nodes = (df[["target", "target_mass", "target_size"]]
                       .rename(columns={"target"      : "id",
                                        "target_mass" : "mass",
                                        "target_size" : "size" }) )
    df_node = pd.concat([ df_source_nodes, df_target_nodes ])
    df_node["title"] = df_node["id"] + " (" + df_node["size"].astype(int).astype(str) + ")"
    df_node = df_node.drop_duplicates().set_index("id")

    # load the graph from an edgelist, and complete it with node attributtes
    G = nx.from_pandas_edgelist(df, edge_attr=["weight", "title"])
    nx.set_node_attributes(G, values=df_node.to_dict(orient="index"))
    return G


def network_analysis_network_wide(G:nx.Graph) -> t.Tuple[nx.Graph, t.Dict]:
    """
    run a network analysis pipeline of the graph G, to compute statistics
    for the whole graph (instead of for each node, done below). return the
    results as a dict

    documentation comes from and cites:
        https://programminghistorian.org/en/lessons/exploring-and-analyzing-network-data-with-python#metrics-available-in-networkx
    """
    # >>> density
    # ratio of actual edges in the network to all possible edges in the network.
    # 1 if all nodes are connected, 0 if no nodes are connected.
    print("")
    print( "* density", nx.density(G) )

    # >>> connectivity
    # true if a path can be made from any node to any node:
    # there is no "separate" subgraph that is not connected to the other nodes
    print( "* connectivity", nx.is_connected(G) )
    # >>> diameter
    # the length of the path between the two nodes
    # that are furthest apart (after calculating all shortest paths between nodes)
    # only works on connected graphs
    if nx.is_connected(G):
        print( "* diameter", nx.diameter(G, weight="weight") )  # type:ignore
    # >>> transitivity
    # the ratio of all triangles over all possible triangles.
    # a triangle represents a triadic closure: if two people know the same person,
    # they are likely to know each other (A and B know C => A and B probably know
    # each other and there is a triangle connecting A,B and C.
    # https://en.wikipedia.org/wiki/Triadic_closure
    # transitivity, like density, expresses how interconnected a graph is in
    # terms of a ratio of actual over possible connections. it expresses how
    # many relationships exist in a graph, compared to all the relations that
    # could exist, but currently do not.
    print( "* transitivity", nx.transitivity(G) )

    stats = { "number_of_nodes": nx.number_of_nodes(G),
              "number_of_edges": nx.number_of_edges(G),
              "density": nx.density(G),
              "transitivity": nx.transitivity(G),
              "connectivity": nx.is_connected(G),
              "diameter": nx.diameter(G) if nx.is_connected(G) else np.nan,
              "number_of_communities": len( community.louvain_communities(G) )  #type:ignore
              }
    return G, stats


def network_analysis_node_wide(G:nx.Graph) -> t.Tuple[nx.Graph, pd.DataFrame]:
    """
    run a network analysis pipeline of graph G at the node-level: the statistics
    differ for each node. return the results in into a pandas dataframe, where each
    node is mapped to the results (eignness centrality, betweenness, community...)

    documentation comes from and cites:
        https://programminghistorian.org/en/lessons/exploring-and-analyzing-network-data-with-python#metrics-available-in-networkx
    """
    # nx often returns a dict of { <node id>: <value> }. transform it into a dataframe.
    # works only on dicts with one value per key (not an arra)
    nxdict_to_df = lambda nxdict, colnames: pd.DataFrame.from_dict({ colnames[0]: list(nxdict.keys())
                                                                   , colnames[1]: list(nxdict.values())
                                                                   }, orient="columns")

    # node-level centrality analysis -------------------------------------------
    #
    # >>> centrality
    # it allows to find which nodes are the most important in a network.
    # three of the most common centrality measures are:
    # degree, betweenness centrality, eigenvector centrality
    #
    # >>> degree
    # the sum of a node's edges.
    # nodes with the highest degree in a social network are the people who know
    # the most people. These nodes are often referred to as hubs, and calculating
    # degree is the quickest way of identifying hubs
    # the degree is a limited measure: it can only identify hubs, and doesn't
    # say much about the other nodes.
    # here, we compute weighted degrees
    degree = dict(G.degree(G.nodes(), weight='weight'))           # type:ignore ; dict of { <node id>: <degree> }
    print("* degrees")
    print(nxdict_to_df(degree, ["id", "degree"]).sort_values(by="degree"))
    nx.set_node_attributes(G, degree, 'degree')
    # >>> eigenvector centrality
    # computes the centrality for a node by adding the centrality of its predecessors.
    # it takes into account if a node is a hub, but also how many hubs the node is
    # connected to. it is useful for understanding which nodes can get information
    # to many other nodes quickly
    eigenvector = nx.eigenvector_centrality(G, weight="weight", max_iter=10000)
    print("* eigenvector centrality")
    print(nxdict_to_df(eigenvector, ["id", "eigenvector"]).sort_values(by="eigenvector"))
    nx.set_node_attributes(G, eigenvector, "eigenvector")
    # >>> betweenness centrality
    # computes the shortest-paths that pass through a certain node.
    # it is a centrality measure that doesn't rely on degrees, but on paths.
    # useful to find the nodes that connect disparate parts of the network.
    # a node with a high betweenness centrality is called a broker:
    # a hub has a lot of connexions, while a broker stands inbetween groups,
    # and is a path between poorly connected groups.
    betweenness = nx.betweenness_centrality(G, weight="weight")
    print("* betweenness centrality")
    print(nxdict_to_df(betweenness, ["id", "betweenness"]).sort_values(by="betweenness"))
    nx.set_node_attributes(G, betweenness, "betweenness")

    print(nx.to_pandas_edgelist(G))  # only prints the edge => all of our calculus is lost.

    # community detection --------------------------------------------------------
    #
    # >>> modularity
    #  a measure of relative density in your network: a community (called a module
    # or modularity class) has high density relative to other nodes within its module
    # but low density with those outside. it partitions the network and shows how
    # united or fractious it is. in a very dense network, a partition won't make sense
    # => USE WITH CAUTION. for themes, it's pretty useless.
    #
    # see:
    #   https://towardsdatascience.com/community-detection-algorithms-9bd8951e7dae
    #
    communities = community.louvain_communities(G, weight="weight")  # based on a Louvain algo, returns t.List, with 1 set per community.
    print(f"* {len(communities)} communities found")
    # print("\n".join(f"{len(list(c))} items in group :::: {str(c)}" for c in communities))  # type:ignore
    groups = { name:i for i,c in enumerate(communities) for name in c }  # dict of `{ <node id>: <group it belongs to> }`
    nx.set_node_attributes(G, groups, "group")


    # to create a df representation of the graph
    df = pd.DataFrame.from_dict({ "degree"      : nx.get_node_attributes(G, "degree")
                                , "eigenvector" : nx.get_node_attributes(G, "eigenvector")
                                , "betweenness" : nx.get_node_attributes(G, "betweenness")
                                , "group"  : nx.get_node_attributes(G, "group")
                                }, orient="columns").reset_index(names="id")

    # we can then compare different measures
    # max_in_groupby = df.groupby("group").betweenness.max().to_list()
    # print(df.loc[ df.betweenness.isin( max_in_groupby ) ])

    df = df.sort_values(by="group")
    return G, df


def vis(G:nx.Graph) -> None:
    """
    visualise graph G
    if visualizing with pyvis, the nodes, edges and network must
    have attributes that correspond to the vis.js data model
    """
    # visualize using pyvis
    net = Network( notebook=False, height="100vh", width="100vw", bgcolor="#3300FF"
                 , select_menu=True)
    net.from_nx(G)

    # set options object according to: https://visjs.github.io/vis-network/docs/network/
    # net.set_options(r"""{}""")

    net.show("example.html", notebook=False)

    # # community grouping graph visualization using netgraph
    # https://stackoverflow.com/a/43541777/17915803
    # dict of { <node id>: <community id> }
    # node_to_community = nx.get_node_attributes(G, "modularity")
    #
    # # dict of `community number` : RGB color ([int, int, int])
    # community_colors = { v: random_color()
    #                      for v in node_to_community.values() }
    #
    # edge_widths = nx.get_node_attributes(G, "weight")
    # node_widths = nx.get_node_attributes(G, "size")
    #
    # # dict of `{ <node_id> : <rgb_color> }`
    # node_colors = { node: community_colors[community_id]
    #                 for node, community_id
    #                 in node_to_community.items()}
    # print(node_colors)
    #
    # #TODO FIX EDGE_WIDTH AND NODE_WIDTH ISSUE
    # # https://netgraph.readthedocs.io/en/latest/graph_classes.html#netgraph.Graph
    # netgraph.Graph(G,
    #                node_color=node_colors,
    #                node_labels=True, node_layout="community", edge_layout="bundled",
    #                node_layout_kwargs={ "node_to_community": node_to_community })
    # plt.show()
    return


def write(df:pd.DataFrame, fp:os.PathLike|str) -> None:
    """
    write the dataframe `df` to the filepath `fp`
    """
    df.to_csv(fp, sep="\t")



def analysis_base_pipeline():
    G_t2t   = read(os.path.join(DATA, "edgelist_theme2theme.csv"))
    G_ne2ne = read(os.path.join(DATA, "edgelist_named_entity2named_entity.csv"))
    G_t2ne  = read(os.path.join(DATA, "edgelist_theme2named_entity.csv"))

    # run network-wide analysis
    G_t2t,   t2t_network_wide   = network_analysis_network_wide(G_t2t)
    G_ne2ne, ne2ne_network_wide = network_analysis_network_wide(G_ne2ne)
    G_t2ne,  t2ne_network_wide  = network_analysis_network_wide(G_t2ne)

    # run node level analysis
    G_t2t,   df_t2t_graph   = network_analysis_node_wide(G_t2t)
    G_ne2ne, df_ne2ne_graph = network_analysis_node_wide(G_ne2ne)
    G_t2ne,  df_t2ne_graph  = network_analysis_node_wide(G_t2ne)

    # output
    # df_network_wide_stats = pd.DataFrame(
    #     [ t2t_network_wide, ne2ne_network_wide, t2ne_network_wide ],
    #     index=["theme2theme", "named_entity2named_entity", "theme2named_entity"])

    write(df_t2t_graph, os.path.join(DATA, "theme2theme.csv"))
    # write(df_t2ne_graph, os.path.join(DATA, "theme2named_entity.csv"))
    # write(df_ne2ne_graph, os.path.join(DATA, "named_entity2named_entity.csv"))
    # write(df_network_wide_stats, os.path.join(DATA, "network_wide_stats.csv"))

    vis(G_t2t)
    vis(G_ne2ne)
    # vis(G_t2ne)



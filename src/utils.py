import numpy as np
import os
import random

SRC           = os.path.abspath(os.path.dirname(__file__))
ROOT          = os.path.join(SRC, os.pardir)
SQL           = os.path.join(SRC, "sql/")
ORM           = os.path.join(SRC, "orm/")
CONFIDENTIALS = os.path.join(ROOT, "confidentials/")
DATA           = os.path.join(ROOT, "data/")


from .db import build_engine
ENGINE = build_engine()


# generate a random color in RGBA. see:  https://stackoverflow.com/a/48793922/17915803
random_color = lambda: [ random.uniform(0,1)
                       , random.uniform(0,1)
                       , random.uniform(0,1)
                       , 1 ]
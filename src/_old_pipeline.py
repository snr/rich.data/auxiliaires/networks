from networkx.algorithms import node_classification
from networkx.algorithms import community
from sqlalchemy.engine import Engine
from pyvis.network import Network
import matplotlib.pyplot as plt
from cdlib import algorithms
from itertools import chain
import networkx as nx
import pandas as pd
import numpy as np
import typing as t
import netgraph
import math
import os


from .utils import SQL, DATA, ENGINE, random_color


def prepare_graph_df_from_sql( engine:Engine
                             , fn_query:str
                             , mapper:t.Dict[str,str]
                             , deduplicate:bool
                             ) -> t.List[pd.DataFrame]:
    """
    process the rows returned by `query` into two dataframes:
    `df_edges` and `df_nodes`, which will be used to create a networkx graph.
    query describes the relationships between all rows of two tables (tableA and tableB),
    where tableA and tableB can be the same (relation between all themes, for examples).

    the SQL query must return the following columns. if other column names
    are returned, provide `mapper` to rename the columns
    table_a_id    : the SQL ID of tableA
    table_a_name  : the `entry_name` of tableA
    table_a_count : the number of times the current row of tableA is used to
                    tag the iconography table
    table_b_id    : the sql ID of tableB
    table_b_name  : the `entry_name` of tableB
    table_b_count : number of times the current row of tableA is used to tag
                    the iconography table
    weight        : the number of relationships between the two rows of tableA
                    and tableB described here.

    if `deduplicate`, drop rows where [ table_b_id, table_a_id ] == [ table_a_id, table_b_id ]:
    when tableA == tableB (the relationship between all rows of a singe table),
    the query generates duplicate rows, one for each direction (tableA->tableB,
    tableB->tableA). when running the query on 2 different tables, no need to deduplicate.

    example query: tableA is 'theme' and tableB is named_entity => we'll
    study all relationships between themes and named entities.
    tableA and tableB can be the same: for example, to study all relationships
    between two themes.

    :param engine: the sqlalchemy engine
    :param fn_query: name of the file containing the query to run.
        the file must be in `./sql/`.
    :param mapper: dict mapping sql column names to df column names
        ({ <sql colname>: <df colname> }) to rename column names returned
        by the SQL query.
    :returns: 2 dataframes
    """
    with open(os.path.join(SQL, fn_query), mode="r") as fh:
        query = fh.read()
    df = pd.read_sql( query, engine ).rename( columns=mapper )

    # when tableA == tableB, 2 rows are added for each relation:
    # tableA -> tableB and tableB -> tableA. this is wrong and
    # useless: we want an undirected graph. delete the rows with
    # a relation from tableB -> tableA.
    if deduplicate:
        df["index_backup"] = df.index  # easier to work with a column than an index
        df["b_to_a"] = df.apply(lambda x: [x["table_b_id"], x["table_a_id"]], axis=1)  # [table_b_id, table_a_id]: a column with the inverse relationship for each row
        todel = []
        def build_todel(row:pd.Series) -> pd.Series:
            """
            `row` describes a relation from tableA to tableB.
            find the inverse relation from tableB to tableA,
            append its index to to_del.
            if we don't use the `if` below, `todel` will contain
            the indexes of all the dataframe
            """
            if row.name not in todel:
                inverse_relation = df.loc[ df.table_a_id.eq(row.b_to_a[0])
                                         & df.table_b_id.eq(row.b_to_a[1])
                                         , "index_backup"
                                         ].to_list()[0]
                todel.append(inverse_relation)
            return row
        df.apply(build_todel, axis=1)
        # len(todel)/df.shape[0] should be equal to 0.5 (if `df` has an even number of rows)
        assert len(todel)/df.shape[0]==0.5       \
               or len(todel)/df.shape[0]+1==0.5  \
               or len(todel)/df.shape[0]-1==0.5, \
               f"invalid number of rows to delete. `todel` should contain half the ids of `df` ({df.shape[0]/2}), got {len(todel)}"
        df = df.loc[ ~df.index_backup.isin(todel) ].drop(columns=["index_backup", "b_to_a"])

    # prepare the dataframes for the graph.
    # 2 dfs for the graph are created: one for the edges, the other for the nodes
    # column names for edges must be pyvis/visjs edge options:
    #   https://pyvis.readthedocs.io/en/latest/documentation.html?highlight=from_nx#pyvis.network.Network.add_edge
    #   https://visjs.github.io/vis-network/docs/network/edges.html
    # column names for nodes must be pyvis node options:
    #   https://visjs.github.io/vis-network/docs/network/nodes.html
    #   https://pyvis.readthedocs.io/en/latest/documentation.html?highlight=from_nx#pyvis.network.Network.add_node
    # to pass data from networkx to pyvis, see: https://stackoverflow.com/q/12182744/17915803

    # function to squeeze numbers on a more readable logarithmic scale.
    # log*5 is much more readable; minimum of 1 to keep things readable
    resizer = lambda x, mul: math.log(x) * mul if math.log(x) * mul >= 1 else 1

    randomids = df.index.to_series()#.sample(n=50)  # sample, else loading time is too long
    df_edges = df.loc[ df.index.isin(randomids) ]
    df_nodes = df.loc[ df.index.isin(randomids) ]

    df_edges["weight_abs"] = df_edges.weight  # store the weight as absolute number
    # df_edges.weight = df_edges.weight.apply(resizer, mul=5)
    df_edges["title"] = ( df_edges.table_a_name + "+" + df_edges.table_b_name + " : "
                        + df_edges.weight_abs.astype(str) + " relations" )
    df_edges = df_edges.rename(columns={ "table_a_name": "from", "table_b_name": "to" })
    df_edges = df_edges[[ 'from','weight','to','title' ]]

    df_nodes = df_nodes.drop_duplicates(subset=["table_a_id"], keep="first")
    df_nodes["id"]    = df.table_a_name
    df_nodes["size"]  = df.table_a_count # / 5
    df_nodes["title"] = df.table_a_name + ": " + df_nodes.table_a_count.astype(str)
    maxsize           = df_nodes["size"].max()
    df_nodes["mass"]  = df_nodes["size"].apply(lambda x: x * (10000/maxsize))  # mass is inversely proportional to size. bigger nodes will have more repulsion
    df_nodes = df_nodes[[ "id","size","title","mass" ]]

    return [ df_edges, df_nodes ]


def df_to_graph(df_edges:pd.DataFrame, df_nodes:pd.DataFrame) -> nx.Graph:
    """
    generate a networkx graph and run calculations

    df_edges has the columns:
        * from   : id of the source node of the edge
        * to     : id of the target node of the edge
        * weight : weight of the edge (number of relation between to and from)
        * title  : name of the edge (visible when hovering over the visualization made using pyvis)
    df_nodes has the columns:
        * id   : the id of the node
        * size : the size of the node (number of times a value is used to tag iconography
        * title: the "name" of the node, visible when hovering over the pyvis visualisation
        * mass : used to compute the repulsion of the node in a pyvis dataviz. the higher
                 the mass, the more repulsion
    """
    assert sorted(df_edges.columns.to_list()) == sorted(['from', 'weight', 'to', 'title'])
    assert sorted(df_nodes.columns.to_list()) == sorted(['id', 'size', 'mass', 'title'])
    assert df_nodes.id.isin(df_edges["from"]).all()

    G = nx.from_pandas_edgelist( df_edges
                               , source="from"
                               , target="to"
                               , edge_attr=["weight", "title"])
    nx.set_node_attributes(G=G, values=df_nodes.set_index("id").to_dict(orient="index") )
    return G


def network_analysis_network_wide(G:nx.Graph) -> t.Dict:
    """
    run a network analysis pipeline of the graph G, to compute statistics
    for the whole graph (instead of for each node, done below). return the
    results as a dict

    documentation comes from and cites:
        https://programminghistorian.org/en/lessons/exploring-and-analyzing-network-data-with-python#metrics-available-in-networkx
    """
    # >>> density
    # ratio of actual edges in the network to all possible edges in the network.
    # 1 if all nodes are connected, 0 if no nodes are connected.
    print("")
    print( "* density", nx.density(G) )

    # >>> connectivity
    # true if a path can be made from any node to any node:
    # there is no "separate" subgraph that is not connected to the other nodes
    print( "* connectivity", nx.is_connected(G) )
    # >>> diameter
    # the length of the path between the two nodes
    # that are furthest apart (after calculating all shortest paths between nodes)
    # only works on connected graphs
    if nx.is_connected(G):
        print( "* diameter", nx.diameter(G, weight="weight") )  # type:ignore
    # >>> transitivity
    # the ratio of all triangles over all possible triangles.
    # a triangle represents a triadic closure: if two people know the same person,
    # they are likely to know each other (A and B know C => A and B probably know
    # each other and there is a triangle connecting A,B and C.
    # https://en.wikipedia.org/wiki/Triadic_closure
    # transitivity, like density, expresses how interconnected a graph is in
    # terms of a ratio of actual over possible connections. it expresses how
    # many relationships exist in a graph, compared to all the relations that
    # could exist, but currently do not.
    print( "* transitivity", nx.transitivity(G) )

    stats = { "number_of_nodes": nx.number_of_nodes(G),
              "number_of_edges": nx.number_of_edges(G),
              "density": nx.density(G),
              "transitivity": nx.transitivity(G),
              "connectivity": nx.is_connected(G),
              "diameter": nx.diameter(G) if nx.is_connected(G) else np.nan,
              "number_of_communities": len( community.louvain_communities(G) )  #type:ignore
              }
    return stats




def network_analysis_node_wide(G:nx.Graph) -> pd.DataFrame:
    """
    run a network analysis pipeline of graph G at the node-level: the statistics
    differ for each node. return the results in into a pandas dataframe, where each
    node is mapped to the results (eignness centrality, betweenness, community...)

    documentation comes from and cites:
        https://programminghistorian.org/en/lessons/exploring-and-analyzing-network-data-with-python#metrics-available-in-networkx
    """
    # nx often returns a dict of { <node id>: <value> }. transform it into a dataframe.
    # works only on dicts with one value per key (not an arra)
    nxdict_to_df = lambda nxdict, colnames: pd.DataFrame.from_dict({ colnames[0]: list(nxdict.keys())
                                                                   , colnames[1]: list(nxdict.values())
                                                                   }, orient="columns")

    # node-level centrality analysis -------------------------------------------
    #
    # >>> centrality
    # it allows to find which nodes are the most important in a network.
    # three of the most common centrality measures are:
    # degree, betweenness centrality, eigenvector centrality
    #
    # >>> degree
    # the sum of a node's edges.
    # nodes with the highest degree in a social network are the people who know
    # the most people. These nodes are often referred to as hubs, and calculating
    # degree is the quickest way of identifying hubs
    # the degree is a limited measure: it can only identify hubs, and doesn't
    # say much about the other nodes.
    # here, we compute weighted degrees
    degree = dict(G.degree(G.nodes(), weight='weight'))           # type:ignore ; dict of { <node id>: <degree> }
    print("* degrees")
    print(nxdict_to_df(degree, ["id", "degree"]).sort_values(by="degree"))
    nx.set_node_attributes(G, degree, 'degree')
    # >>> eigenvector centrality
    # computes the centrality for a node by adding the centrality of its predecessors.
    # it takes into account if a node is a hub, but also how many hubs the node is
    # connected to. it is useful for understanding which nodes can get information
    # to many other nodes quickly
    eigenvector = nx.eigenvector_centrality(G, weight="weight", max_iter=10000)
    print("* eigenvector centrality")
    print(nxdict_to_df(eigenvector, ["id", "eigenvector"]).sort_values(by="eigenvector"))
    nx.set_node_attributes(G, eigenvector, "eigenvector")
    # >>> betweenness centrality
    # computes the shortest-paths that pass through a certain node.
    # it is a centrality measure that doesn't rely on degrees, but on paths.
    # useful to find the nodes that connect disparate parts of the network.
    # a node with a high betweenness centrality is called a broker:
    # a hub has a lot of connexions, while a broker stands inbetween groups,
    # and is a path between poorly connected groups.
    betweenness = nx.betweenness_centrality(G, weight="weight")
    print("* betweenness centrality")
    print(nxdict_to_df(betweenness, ["id", "betweenness"]).sort_values(by="betweenness"))
    nx.set_node_attributes(G, betweenness, "betweenness")

    print(nx.to_pandas_edgelist(G))  # only prints the edge => all of our calculus is lost.

    # community detection --------------------------------------------------------
    #
    # >>> modularity
    #  a measure of relative density in your network: a community (called a module
    # or modularity class) has high density relative to other nodes within its module
    # but low density with those outside. it partitions the network and shows how
    # united or fractious it is. in a very dense network, a partition won't make sense
    # => USE WITH CAUTION. for themes, it's pretty useless.
    #
    # see:
    #   https://towardsdatascience.com/community-detection-algorithms-9bd8951e7dae
    #
    communities = community.louvain_communities(G, weight="weight")  # based on a Louvain algo, returns t.List, with 1 set per community.
    print(f"* {len(communities)} communities found")
    # print("\n".join(f"{len(list(c))} items in group :::: {str(c)}" for c in communities))  # type:ignore
    groups = { name:i for i,c in enumerate(communities) for name in c }  # dict of `{ <node id>: <group it belongs to> }`
    nx.set_node_attributes(G, groups, "modularity")


    # to create a df representation of the graph
    df = pd.DataFrame.from_dict({ "degree"      : nx.get_node_attributes(G, "degree")
                                , "eigenvector" : nx.get_node_attributes(G, "eigenvector")
                                , "betweenness" : nx.get_node_attributes(G, "betweenness")
                                , "modularity"  : nx.get_node_attributes(G, "modularity")
                                }, orient="columns").reset_index(names="id")

    # we can then compare different measures
    # max_in_groupby = df.groupby("modularity").betweenness.max().to_list()
    # print(df.loc[ df.betweenness.isin( max_in_groupby ) ])

    df = df.sort_values(by="modularity")
    return df


def vis(G:nx.Graph) -> None:
    """visualise graph G"""
    # visualize using pyvis
    # net = Network(notebook=False, height="100vh", width="100vw", bgcolor="#3300FF")
    # net.from_nx(G, edge_weight_transf=lambda x: 1)

    # net.show("example.html", notebook=False)

    # community grouping graph vis
    # https://stackoverflow.com/a/43541777/17915803
    # dict of { <node id>: <community id> }
    node_to_community = nx.get_node_attributes(G, "modularity")

    # dict of `community number` : RGB color ([int, int, int])
    community_colors = { v: random_color()
                         for v in node_to_community.values() }

    # edge_widths = nx.get_node_attributes(G, "weight")

    node_widths = nx.get_node_attributes(G, "size")

    # dict of `{ <node_id> : <rgb_color> }`
    node_colors = { node: community_colors[community_id]
                    for node, community_id
                    in node_to_community.items()}
    print(node_colors)

    #TODO FIX EDGE_WIDTH AND NODE_WIDTH ISSUE
    #https://netgraph.readthedocs.io/en/latest/graph_classes.html#netgraph.Graph
    netgraph.Graph(G,
                   node_color=node_colors,
                   node_labels=True, node_layout="community", edge_layout="bundled",
                   node_layout_kwargs={ "node_to_community": node_to_community })

    plt.show()
    return


def write(df:pd.DataFrame, fp:os.PathLike|str) -> None:
    """
    write the dataframe `df` to the filepath `fp`
    """
    df.to_csv(fp, sep="\t")


def pipeline():
    """
    """

    # build the graphs
    mapper = { "theme_a_id": "table_a_id"      , "theme_b_id": "table_b_id",
               "theme_a_name": "table_a_name"  , "theme_b_name": "table_b_name",
               "theme_a_count": "table_a_count", "theme_b_count": "table_b_count",
               "weight": "weight" }
    df_theme_edges, df_theme_nodes = prepare_graph_df_from_sql( ENGINE
                                                              , "theme2theme.sql"
                                                              , mapper
                                                              , True)
    mapper = { "named_entity_a_id": "table_a_id"      , "named_entity_b_id": "table_b_id",
               "named_entity_a_name": "table_a_name"  , "named_entity_b_name": "table_b_name",
               "named_entity_a_count": "table_a_count", "named_entity_b_count": "table_b_count",
               "weight": "weight" }
    df_ne_edges, df_ne_nodes = prepare_graph_df_from_sql( ENGINE
                                                        , "named_entity2named_entity.sql"
                                                        , mapper
                                                        , True)
    mapper = { "theme_id": "table_a_id"      , "named_entity_id": "table_b_id",
               "theme_name": "table_a_name"  , "named_entity_name": "table_b_name",
               "theme_count": "table_a_count", "named_entity_count": "table_b_count",
               "weight": "weight" }
    df_t2ne_edges, df_t2ne_nodes = prepare_graph_df_from_sql( ENGINE
                                                            , "theme2named_entity.sql"
                                                            , mapper
                                                            , False)

    G_t2t   = df_to_graph(df_theme_edges, df_theme_nodes)
    # G_ne2ne = df_to_graph(df_ne_edges, df_ne_nodes)
    # G_t2ne  = df_to_graph(df_t2ne_edges, df_t2ne_nodes)

    # run network-wide analysis
    # t2t_network_wide   = network_analysis_network_wide(G_t2t)
    # ne2ne_network_wide = network_analysis_network_wide(G_ne2ne)
    # t2ne_network_wide  = network_analysis_network_wide(G_t2ne)

    # run node level analysis
    df_t2t_graph   = network_analysis_node_wide(G_t2t)
    # df_ne2ne_graph = network_analysis_node_wide(G_ne2ne)
    # df_t2ne_graph  = network_analysis_node_wide(G_t2ne)

    # output
    # df_network_wide_stats = pd.DataFrame(
    #     [ t2t_network_wide, ne2ne_network_wide, t2ne_network_wide ],
    #     index=["theme2theme", "named_entity2named_entity", "theme2named_entity"])

    write(df_t2t_graph, os.path.join(DATA, "theme2theme.csv"))
    #write(df_t2ne_graph, os.path.join(DATA, "theme2named_entity.csv"))
    #write(df_ne2ne_graph, os.path.join(DATA, "named_entity2named_entity.csv"))
    #write(df_network_wide_stats, os.path.join(DATA, "network_wide_stats.csv"))

    vis(G_t2t)
    # vis(G_ne2ne)
    # vis(G_t2ne)

    return



#############################"
#
#
#
# old way to make a simpler query with more pre-process in `prepare_graph_df_from_sql`
# the structure outputted by this pipeline is the same as what's obtained
# directly by the current query
#
# def prepare_graph_df_from_sql(engine:Engine) -> t.List[pd.DataFrame]:
#     """
#     process the rows returned by `sql_theme_rel` into two dataframes:
#     `df_edges` and `df_nodes`, which will be used to create a networkx graph.
#
#     basically, each row of the query documents the relationship
#     between a main theme (called `themeA` below) and the other
#     themes in the database (all called `themeB`) in order to create
#     a non-directionnal graph containing all themes together
#
#     output columns:
#     * theme_id: int. the PK/id of themeA
#     * theme_name: str. the name of themeA
#     * associated_theme: t.List[t.Dict]: an array of associated themes (themeB)
#          each themeB is a dict of { theme_id: <id of themeB>,
#                                     theme_name: name of themeB>,
#                                     weight: <number of links from themeA to themeB> }
#     """
#     # relations between all themes
#     sql_theme_rel = """WITH icono2theme AS (
#                       SELECT r_iconography_theme.id_iconography
#                            , json_agg(json_build_object( 'theme_id', r_iconography_theme.id_theme
#                                                        , 'theme_name', theme.entry_name )
#                                      ) AS theme_from_icono
#                       FROM r_iconography_theme
#                       JOIN theme ON r_iconography_theme.id_theme = theme.id
#                       GROUP BY r_iconography_theme.id_iconography
#                     )
#                     SELECT r_iconography_theme.id_theme AS theme_id
#                          , theme.entry_name AS theme_name
#                          , json_agg(icono2theme.theme_from_icono) AS associated_themes
#                     FROM r_iconography_theme
#                     JOIN icono2theme
#                     ON icono2theme.id_iconography = r_iconography_theme.id_iconography
#                     JOIN theme ON r_iconography_theme.id_theme = theme.id
#                     GROUP BY r_iconography_theme.id_theme, theme.entry_name
#                     ;"""
#     # theme mapped to number of occurrences
#     sql_theme_count = """SELECT theme.id AS theme_id
#                               , theme.entry_name AS theme_name
#                               , COUNT(r_iconography_theme.id) AS theme_count
#                          FROM theme
#                          JOIN r_iconography_theme ON r_iconography_theme.id_theme = theme.id
#                          GROUP BY theme.id, theme.entry_name
#                          ORDER BY theme_count DESC;"""
#
#     df = pd.read_sql(sql_theme_rel, engine)
#     df_count = pd.read_sql(sql_theme_count, engine)
#
#     # unnsest associated_themes. each theme is now mapped to a non-deduplicated list of
#     # { "theme_id": <int>, "theme_name": <name of the theme (str)> }.
#     df.associated_themes = df.associated_themes.apply( lambda x: list(chain(*x)) )
#
#     # remove relations from a theme to itself
#     # (items in `df.associated_themes` where "theme_id" == df.theme_id)
#     df.associated_themes = df.apply(lambda x: [ d
#                                                 for d in x.associated_themes
#                                                 if d["theme_id"] != x.name ], axis=1)
#
#     # drop rows with no relation
#     print(f"* dropping { df.associated_themes.apply(len).gt(0).sum() } row(s) with no relations. ")
#     print(f"* names of rows dropped: { df.loc[df.associated_themes.apply(len).eq(0), 'theme_name'].to_list() }")
#     df = df.loc[df.associated_themes.apply(len).gt(0)]
#
#     # replace duplicates in df.associated_theme by a weight
#     # (count of number of relations) between themeA and themeB
#     # df.associated_themes is now an array of
#     # { theme_id: <int>, theme_name: <theme name>, count: <number of entries> }
#     def dedup_and_weight(row:pd.Series) -> t.List[t.Dict] | None:
#         """
#         each `df.associated_themes` cell is transformed into a df.
#         duplicate values are grouped together, and a weight (number of duplicates)
#         is calculated for each value associated to a theme.
#         """
#         df_associated = pd.DataFrame.from_dict(row.associated_themes)
#         df_associated_dedup = (df_associated.groupby(["theme_id"])  #type:ignore
#                                             .first()
#                                             .reset_index(names="theme_id"))
#         df_associated_dedup["weight"] = (df_associated_dedup
#                                          .theme_id
#                                          .apply(lambda x: df_associated.theme_id.eq(x).sum()) )
#         return df_associated_dedup.to_dict("records")
#
#     df.associated_themes = df.apply(dedup_and_weight, axis=1)   #type:ignore
#     df = df.explode("associated_themes")
#     df = df.rename(columns={"theme_id": "theme_a_id", "theme_name": "theme_a_name"})
#
#     df["weight"]       = df.associated_themes.apply(lambda x: x["weight"])
#     df["theme_b_id"]   = df.associated_themes.apply(lambda x: x["theme_id"])
#     df["theme_b_name"] = df.associated_themes.apply(lambda x: x["theme_name"])
#     df = df.drop("associated_themes", axis=1).reset_index(drop=True)

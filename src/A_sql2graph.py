"""
from an SQL query, generate a networkX graph and write it to file
"""

import networkx as nx
import pandas as pd
import typing as t
import math
import os

from .utils import DATA, SQL, ENGINE
from .db import Engine


def prepare_graph_df_from_sql( engine:Engine
                             , fn_query:str
                             , mapper:t.Dict[str,str]
                             , deduplicate:bool
                             ) -> t.List[pd.DataFrame]:
    """
    process the rows returned by `query` into two dataframes:
    `df_edges` and `df_nodes`, which will be used to create a networkx graph.
    query describes the relationships between all rows of two tables (tableA and tableB),
    where tableA and tableB can be the same (relation between all themes, for examples).

    the SQL query must return the following columns. if other column names
    are returned, provide `mapper` to rename the columns
    table_a_id    : the SQL ID of tableA
    table_a_name  : the `entry_name` of tableA
    table_a_count : the number of times the current row of tableA is used to
                    tag the iconography table
    table_b_id    : the sql ID of tableB
    table_b_name  : the `entry_name` of tableB
    table_b_count : number of times the current row of tableA is used to tag
                    the iconography table
    weight        : the number of relationships between the two rows of tableA
                    and tableB described here.

    if `deduplicate`, drop rows where [ table_b_id, table_a_id ] == [ table_a_id, table_b_id ]:
    when tableA == tableB (the relationship between all rows of a singe table),
    the query generates duplicate rows, one for each direction (tableA->tableB,
    tableB->tableA). when running the query on 2 different tables, no need to deduplicate.

    example query: tableA is 'theme' and tableB is named_entity => we'll
    study all relationships between themes and named entities.
    tableA and tableB can be the same: for example, to study all relationships
    between two themes.

    :param engine: the sqlalchemy engine
    :param fn_query: name of the file containing the query to run.
        the file must be in `./sql/`.
    :param mapper: dict mapping sql column names to df column names
        ({ <sql colname>: <df colname> }) to rename column names returned
        by the SQL query.
    :returns: 2 dataframes (see `df_to_graph()` for structure)
    """
    with open(os.path.join(SQL, fn_query), mode="r") as fh:
        query = fh.read()
    df = pd.read_sql( query, engine ).rename( columns=mapper )

    # when tableA == tableB, 2 rows are added for each relation:
    # tableA -> tableB and tableB -> tableA. this is wrong and
    # useless: we want an undirected graph. delete the rows with
    # a relation from tableB -> tableA.
    if deduplicate:
        df["index_backup"] = df.index  # easier to work with a column than an index
        df["b_to_a"] = df.apply(lambda x: [x["table_b_id"], x["table_a_id"]], axis=1)  # [table_b_id, table_a_id]: a column with the inverse relationship for each row
        todel = []
        def build_todel(row:pd.Series) -> pd.Series:
            """
            `row` describes a relation from tableA to tableB.
            find the inverse relation from tableB to tableA,
            append its index to to_del.
            if we don't use the `if` below, `todel` will contain
            the indexes of all the dataframe
            """
            if row.name not in todel:
                inverse_relation = df.loc[ df.table_a_id.eq(row.b_to_a[0])
                                         & df.table_b_id.eq(row.b_to_a[1])
                                         , "index_backup"
                                         ].to_list()[0]
                todel.append(inverse_relation)
            return row
        df.apply(build_todel, axis=1)
        # len(todel)/df.shape[0] should be equal to 0.5 (if `df` has an even number of rows)
        assert len(todel)/df.shape[0]==0.5       \
               or len(todel)/df.shape[0]+1==0.5  \
               or len(todel)/df.shape[0]-1==0.5, \
               f"invalid number of rows to delete. `todel` should contain half the ids of `df` ({df.shape[0]/2}), got {len(todel)}"
        df = df.loc[ ~df.index_backup.isin(todel) ].drop(columns=["index_backup", "b_to_a"])

    # prepare the dataframes for the graph.
    # 2 dfs for the graph are created: one for the edges, the other for the nodes
    # column names for edges must be pyvis/visjs edge options:
    #   https://pyvis.readthedocs.io/en/latest/documentation.html?highlight=from_nx#pyvis.network.Network.add_edge
    #   https://visjs.github.io/vis-network/docs/network/edges.html
    # column names for nodes must be pyvis node options:
    #   https://visjs.github.io/vis-network/docs/network/nodes.html
    #   https://pyvis.readthedocs.io/en/latest/documentation.html?highlight=from_nx#pyvis.network.Network.add_node
    # to pass data from networkx to pyvis, see: https://stackoverflow.com/q/12182744/17915803

    # function to squeeze numbers on a more readable logarithmic scale.
    # log*5 is much more readable; minimum of 1 to keep things readable
    resizer = lambda x, mul: math.log(x) * mul if math.log(x) * mul >= 1 else 1

    # to build df_nodes, concatenate 2 dataframes: one for the "from" nodes,
    # the other one for the "to" nodes
    df_edges = df
    df_nodes_source = (df[["table_a_id", "table_a_name", "table_a_count"]]
                       .rename(columns={ "table_a_name" : "table_name",
                                         "table_a_count": "table_count" }))
    df_nodes_target = (df[["table_b_id", "table_b_name", "table_b_count"]]
                       .rename(columns={ "table_b_name" : "table_name",
                                         "table_b_count": "table_count"}))
    df_nodes = pd.concat([df_nodes_source, df_nodes_target])

    # clean df_edges
    df_edges["weight_abs"] = df_edges.weight  # store the weight as absolute number
    df_edges.weight        = df_edges.weight#.apply(resizer, mul=3)
    df_edges["title"]      = ( df_edges.table_a_name + "+" + df_edges.table_b_name
                             + " : " + df_edges.weight_abs.astype(str) + " relations" )
    df_edges = df_edges.rename(columns={ "table_a_name": "from", "table_b_name": "to" })
    df_edges = df_edges[[ 'from','weight','to','title' ]]

    # clean df_nodes
    df_nodes          = df_nodes.drop_duplicates(subset="table_name", keep="first")
    df_nodes["id"]    = df_nodes.table_name
    df_nodes["title"] = df_nodes.table_name + ": " + df_nodes.table_count.astype(str)
    df_nodes["size"]  = df_nodes.table_count#.apply(resizer, mul=5)
    maxsize           = df_nodes["size"].max()
    df_nodes["mass"]  = df_nodes["size"].apply(lambda x: x * (10000/maxsize))  # mass is inversely proportional to size. bigger nodes will have more repulsion
    df_nodes = df_nodes[[ "id","size","title","mass" ]]

    return [ df_edges, df_nodes ]


def df_to_graph(df_edges:pd.DataFrame, df_nodes:pd.DataFrame) -> nx.Graph:
    """
    generate a networkx graph and run calculations

    df_edges has the columns:
        * from   : id of the source node of the edge
        * to     : id of the target node of the edge
        * weight : weight of the edge (number of relation between to and from)
        * title  : name of the edge (visible when hovering over the visualization made using pyvis)
    df_nodes has the columns:
        * id   : the id of the node
        * size : the size of the node (number of times a value is used to tag iconography
        * title: the "name" of the node, visible when hovering over the pyvis visualisation
        * mass : used to compute the repulsion of the node in a pyvis dataviz. the higher
                 the mass, the more repulsion
    """
    assert sorted(df_edges.columns.to_list()) == sorted(['from', 'weight', 'to', 'title'])
    assert sorted(df_nodes.columns.to_list()) == sorted(['id', 'size', 'mass', 'title'])
    assert df_edges["from"].isin(df_nodes.id).all() and df_edges.to.isin(df_nodes.id).all()

    G = nx.from_pandas_edgelist( df_edges
                               , source="from"
                               , target="to"
                               , edge_attr=["weight", "title"])
    nx.set_node_attributes(G=G, values=df_nodes.set_index("id").to_dict(orient="index") )
    return G


def write(G, fp) -> None:
    """
    write the graph G to file.
    nx.to_pandas_edgelist() returns an edge list (duh), so it means that all
    info regarding nodes in dropped. to avoid this, we add manually node data
    in columns.
    when building graphs from DFs written here, node info will need to be
    manually added.

    :param G: the graph to write
    :param fp: the path to write to
    """
    df           = nx.to_pandas_edgelist(G)
    mass: t.Dict = nx.get_node_attributes(G, "mass")
    size: t.Dict = nx.get_node_attributes(G, "size")

    # add node data
    colnames = ["source_mass", "target_mass", "source_size", "target_size"]
    df[colnames] = df.apply(lambda x: pd.Series([ mass[x.source]
                                                , mass[x.target]
                                                , size[x.source]
                                                , size[x.target] ]), axis=1)
    df.to_csv(fp, sep="\t", index_label="index")
    return


def sql2graph_pipeline():
    """"""
    # build the graphs
    mapper = { "theme_a_id": "table_a_id"      , "theme_b_id": "table_b_id",
               "theme_a_name": "table_a_name"  , "theme_b_name": "table_b_name",
               "theme_a_count": "table_a_count", "theme_b_count": "table_b_count",
               "weight": "weight" }
    df_theme_edges, df_theme_nodes = prepare_graph_df_from_sql( ENGINE
                                                              , "theme2theme.sql"
                                                              , mapper
                                                              , True)
    mapper = { "named_entity_a_id": "table_a_id"      , "named_entity_b_id": "table_b_id",
               "named_entity_a_name": "table_a_name"  , "named_entity_b_name": "table_b_name",
               "named_entity_a_count": "table_a_count", "named_entity_b_count": "table_b_count",
               "weight": "weight" }
    df_ne_edges, df_ne_nodes = prepare_graph_df_from_sql( ENGINE
                                                        , "named_entity2named_entity.sql"
                                                        , mapper
                                                        , True)
    mapper = { "theme_id": "table_a_id"      , "named_entity_id": "table_b_id",
               "theme_name": "table_a_name"  , "named_entity_name": "table_b_name",
               "theme_count": "table_a_count", "named_entity_count": "table_b_count",
               "weight": "weight" }
    df_t2ne_edges, df_t2ne_nodes = prepare_graph_df_from_sql( ENGINE
                                                            , "theme2named_entity.sql"
                                                            , mapper
                                                            , False)

    G_t2t   = df_to_graph(df_theme_edges, df_theme_nodes)
    G_ne2ne = df_to_graph(df_ne_edges, df_ne_nodes)
    G_t2ne  = df_to_graph(df_t2ne_edges, df_t2ne_nodes)

    write(G_t2t, os.path.join(DATA, "edgelist_theme2theme.csv"))
    write(G_ne2ne, os.path.join(DATA, "edgelist_named_entity2named_entity.csv"))
    write(G_t2ne, os.path.join(DATA, "edgelist_theme2named_entity.csv"))

    return
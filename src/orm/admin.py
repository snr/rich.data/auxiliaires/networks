from sqlalchemy.orm import Mapped, mapped_column, relationship, validates
from sqlalchemy.dialects import postgresql as psql
from sqlalchemy import Text
from typing import List

from ..utils.strings import _validate_uuid
from . import Base


# -----------------------------------------------------------------
# tables relative to data administration
#
# contains
# ~~~~~~~~
# * `Institution`: the institution granting access to a digitized
#   ressource
# * `Licence`: licences of the ressources we created and of the
#   digitized documents made available by other institutions
# * `AdminPerson`: researchers and members of the project
# -----------------------------------------------------------------


class Institution(Base):
    """
    the institutions that own and/or grant access to a digitized
    ressource (Iconography, Cartography, Bottins).
    """
    __tablename__ = "institution"

    id            : Mapped[int] = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid       : Mapped[str] = mapped_column(Text, nullable=False)
    entry_name    : Mapped[str] = mapped_column(Text, nullable=False)
    description   : Mapped[str] = mapped_column(Text, nullable=True)

    r_institution : Mapped[List["R_Institution"]] = relationship("R_Institution"
                                                                 , back_populates="institution")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, value):
        return _validate_uuid(value, self.__tablename__)


class Licence(Base):
    """
    licence under which a ressource is made available.
    this table is used to describe both:
    * the licence under which we make the data we produced available
    * the licences under which the digitized documents (image of paintings,
      maps...) are provided by other institutions
    """
    __tablename__ = "licence"

    id          : Mapped[int] = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid     : Mapped[str] = mapped_column(Text, nullable=False)
    entry_name  : Mapped[str] = mapped_column(Text, nullable=False)
    description : Mapped[str] = mapped_column(Text, nullable=True)

    filename    : Mapped[List["Filename"]]        = relationship("Filename", back_populates="licence")
    iconography : Mapped[List["Iconography"]] = relationship("Iconography", back_populates="licence")
    cartography : Mapped[List["Cartography"]] = relationship("Cartography", back_populates="licence")
    directory   : Mapped[List["Directory"]]   = relationship("Directory", back_populates="licence")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)


class AdminPerson(Base):
    """
    members of the Richelieu. Histoire du quartier research project.
    this table allows us to attribute authors to the ressources we
    made available. keeping in line with open data objectives, each
    researcher is identified by a persistent identifier (ORCID...)
    """
    __tablename__ = "admin_person"

    id             : Mapped[int] = mapped_column(psql.INTEGER, nullable=False, primary_key=True)
    id_uuid        : Mapped[str] = mapped_column(Text, nullable=False)
    first_name     : Mapped[str] = mapped_column(Text, nullable=False)
    last_name      : Mapped[str] = mapped_column(Text, nullable=False)
    id_persistent  : Mapped[int] = mapped_column(Text, nullable=True)

    r_admin_person : Mapped[List["R_AdminPerson"]] = relationship("R_AdminPerson"
                                                                  , back_populates="admin_person")

    @validates("id_uuid", include_backrefs=False)
    def validate_uuid(self, key, _uuid):
        return _validate_uuid(_uuid, self.__tablename__)



from sqlalchemy import Engine, create_engine
import json
import os

from .utils import CONFIDENTIALS

def build_engine() -> Engine:
    """
    create an SQLAlchemy PostgreSQL engine.
    credentials are stated in a file in ../confidentials/,
    with the structure:
    >>> {
    ...   "username": "...",
    ...   "password": "...",
    ...   "uri": "...",
    ...   "db": "..."
    ... }

    """
    # read the credentials
    fn = "postgresql_credentials.json"
    with open(os.path.join(CONFIDENTIALS, fn)) as fh:
        cred = json.load(fh)

    # create the engine
    return create_engine(
        f"postgresql://{ cred['username'] }:{ cred['password'] }@{ cred['uri'] }/{ cred['db'] }"
        , echo=True
    )

# NETWORKS

Analyse de réseaux et de graphes de la base de données RichData.

---

## Utilisation

Avoir un fichier `./confidentials/postgresql_credentials.json` avec les identifiants de
connexion à la base postgres: 

```python
{
  "username": "nom d'utilisateurice", 
  "password": "code d'accès",
  "uri": "uri de connexion à la base, ou localhost",
  "db": "nom de la base"
}
```

Lancer les commandes suivantes:

```bash
source env/bin/activate
pip install -r requirements.txt
python main.py
```

Les résultats de l'analyse (tableurs CSV contenant des statistiques 
sur les différentes relations) est conservé dans `out/`.

---

## LICENSE 

GNU GPL 3.0, données CC BY 4.0
